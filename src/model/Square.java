package model;

import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;

import pieces.Piece;
import control.SquareControl;

public class Square {

	public interface SquareEventListener {
		public void onOutEvent(Square square);

		public void onHoverEvent(Square square);

		public void onSelectEvent(Square square);
	}

	public interface SquareChangeListener {
		public void onColorChange(Square square);

		public void onChangeImagePath(Square square);
	}

	public static final int DEFAULT_SIZE = 64;
	public static final String EMPTY_PATH = "";
	public static final Color DEFAULT_COLOR = Color.WHITE;
	public static final Color POSSIB_COL_ONE = new Color(50, 100, 200); // poss�veis locais para se mover
	public static final Color POSSIB_COL_TWO = new Color(20, 50, 150); // poss�veis locais para se mover
	public static final Color POSSIB_COL_TREE = new Color(50, 200, 100); // mover a lugar possivel
	public static final Color LOSER_COL_ONE = new Color(220, 10, 60); // cor das pe�as perdedoras
	public static final Color LOSER_COL_TWO = new Color(180, 10, 50); // cor das pe�as perdedoras
	public static final SquareEventListener EMPTY_EVENT_LISTENER = null;
	public static final SquareChangeListener EMPTY_CHANGE_LISTENER = null;

	private Color color;
	private Integer size;
	private Point position;
	private String imagePath;

	private SquareEventListener squareEventListener;
	private SquareChangeListener squareChangeListener;
	
	// Pe�a atualmente sobre o quadrado
	private Piece pieceAtual = null;

	public Piece getPieceAtual() {
		return pieceAtual;
	}

	public void setPieceAtual(Piece pieceAtual) {
		this.pieceAtual = pieceAtual;
	}

	public Square() {
		this(0, 0);
	}

	public Square(int x, int y) {
		this(new Point(x, y));
	}

	public Square(Point position) {
		this(position, DEFAULT_COLOR);
	}

	public Square(int x, int y, Color color) {
		this(x, y, color, EMPTY_PATH);
	}

	public Square(Point position, Color color) {
		this(position, color, EMPTY_PATH);
	}

	public Square(int x, int y, Color color, int size) {
		this(x, y, color, EMPTY_PATH, size);
	}

	public Square(Point position, Color color, int size) {
		this(position, color, EMPTY_PATH, size);
	}

	public Square(int x, int y, Color color, String imagePath) {
		this(new Point(x, y), color, imagePath, DEFAULT_SIZE);
	}

	public Square(Point position, Color color, String imagePath) {
		this(position, color, imagePath, DEFAULT_SIZE);
	}

	public Square(int x, int y, Color color, String imagePath, int size) {
		this(new Point(x, y), color, imagePath, size);
	}

	public Square(Point position, Color color, String imagePath, int size) {
		this.size = size;
		this.color = color;
		this.position = position;
		this.imagePath = imagePath;

		this.squareEventListener = EMPTY_EVENT_LISTENER;
	}

	public void removeImage() {
		this.imagePath = EMPTY_PATH;
		notifyOnChangeImagePath();
	}

	public void notifyOnOutEvent() {
		if (haveSquareEventListener()) {
			this.squareEventListener.onOutEvent(this);
		}
	}

	public void notifyOnHoverEvent() {
		if (haveSquareEventListener()) {
			this.squareEventListener.onHoverEvent(this);
		}
	}

	public void notifyOnSelectEvent() {
		if (haveSquareEventListener()) {
			this.squareEventListener.onSelectEvent(this);
		}
	}

	public void notifyOnColorChange() {
		if (haveSquareChangeListener()) {
			this.squareChangeListener.onColorChange(this);
		}
	}

	public void notifyOnChangeImagePath() {
		if (haveSquareChangeListener()) {
			this.squareChangeListener.onChangeImagePath(this);
		}
	}

	public Point getPosition() {
		return this.position;
	}

	public Integer getSize() {
		return this.size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}

	public boolean haveImagePath() {
		return this.imagePath != EMPTY_PATH;
	}

	public String getImagePath() {
		return this.imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
		notifyOnChangeImagePath();
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
		notifyOnColorChange();
	}

	public void setSquareEventListener(SquareEventListener squareEventListener) {
		this.squareEventListener = squareEventListener;
	}

	public void setSquareChangeListener(
			SquareChangeListener squareChangeListener) {
		this.squareChangeListener = squareChangeListener;
	}

	private boolean haveSquareEventListener() {
		return this.squareEventListener != EMPTY_EVENT_LISTENER;
	}

	private boolean haveSquareChangeListener() {
		return this.squareChangeListener != EMPTY_CHANGE_LISTENER;
	}
	
	// Set de cor de nega��o personalizada segundo a cor default do Square
	public void makeNotColor() {
		if((this.position.x + this.position.y)%2 == 0) // Squares brancos
			this.setColor(SquareControl.DEFAULT_COLOR_NOT_ONE);
		else // Squares pretos
			this.setColor(SquareControl.DEFAULT_COLOR_NOT_TWO);
	}
	
	// Retorno da lista de Squares entre dois informados
	public static ArrayList<Square> squareBetween(Square sqrOne, Square sqrTwo,
			SquareControl sqrControl) {
		ArrayList<Square> list = new ArrayList<Square>();
		
		// verificar alinhamento:
		if(sqrOne.position.x == sqrTwo.position.x) { // alinhamento horizontal
			for(int i=0; i<8; i++) {
				if((i<sqrOne.position.y && i>sqrTwo.position.y) ||
						(i<sqrTwo.position.y && i>sqrOne.position.y)) {
					list.add(sqrControl.getSquare(sqrOne.position.x, i));
				}
			}
		} else if(sqrOne.position.y == sqrTwo.position.y) { // alinhamento vertical
			for(int i=0; i<8; i++) {
				if((i<sqrOne.position.x && i>sqrTwo.position.x) ||
						(i<sqrTwo.position.x && i>sqrOne.position.x)) {
					list.add(sqrControl.getSquare(i, sqrOne.position.y));
				}
			}
		} else if(Math.abs(sqrOne.position.x-sqrTwo.position.x) ==
				Math.abs(sqrOne.position.y-sqrTwo.position.y)) { // alinhamento diagonal

			int x = sqrOne.position.x > sqrTwo.position.x? 1 : -1;
			int y = sqrOne.position.y > sqrTwo.position.y? 1 : -1;

			for(int i=1; i<Math.abs(sqrOne.position.x-sqrTwo.position.x); i++) {
				list.add(sqrControl.getSquare(sqrTwo.position.x + (i*x),
						sqrTwo.position.y + (i*y)));
			}
		}

		return list;
	}

	// set de cor de movimento possivel
	public void makePossibCol() {
		if((this.position.x + this.position.y)%2 == 0) // Squares brancos
			this.setColor(Square.POSSIB_COL_ONE);
		else // Squares pretos
			this.setColor(Square.POSSIB_COL_TWO);
	}
	
	// set de cor de pe�as que perderam
	public void makeLoserCol() {
		if((this.position.x + this.position.y)%2 == 0) // Squares brancos
			this.setColor(Square.LOSER_COL_ONE);
		else // Squares pretos
			this.setColor(Square.LOSER_COL_TWO);
	}
}
