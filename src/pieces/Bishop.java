package pieces;

import model.Square;

public class Bishop extends Piece { // BISPOS

	/*--------------------------------------------------------------------------------------*/
	// M�TODOS

	// construtor
	public Bishop(Square squareAtual, int group) {
		super(squareAtual, group);
		this.setSquareImage("B");
	}

	@Override
	protected void moveableUpdate() {
		// movimento do bispo
		Bishop.bishopMoving(this, 8); // todo o tabuleiro
	}
	
	// movimento do bispo (modularizado por conta da rainha e do rei)
	public static void bishopMoving(Piece piece, int ext) {

		// listagem das possibilidades para piece pelas propriedades dos bispos
		for(int i=0; i<4; i++) {
			int a, b; // auxiliares de marca��o

			a = 2*(int)(i/2) - 1;
			b = 2*(i%2) -1;

			for(int j=1; j<ext; j++) {
				if(piece.squareAtual.getPosition().x+(j*a) > 7 ||
						piece.squareAtual.getPosition().x+(j*a) < 0 ||
						piece.squareAtual.getPosition().y+(j*b) > 7 ||
						piece.squareAtual.getPosition().y+(j*b) < 0) break; // fora do tabuleiro

				Piece.target = Piece.getSquareControl().getSquare(
						piece.squareAtual.getPosition().x+(j*a),
						piece.squareAtual.getPosition().y+(j*b));

				if(Piece.target.getPieceAtual() != null) { // existe uma pe�a no local
					if(piece.getGroup()*Piece.target.getPieceAtual().getGroup() == -1) {
						// pe�a inimiga
						piece.lPos.add(Piece.target);
					} else {
						// pe�a amiga
						piece.lAtackPos.add(Piece.target);
					}
					break;
				}
				// n�o existe pe�a no local
				piece.lPos.add(Piece.target);
			}	
		}
		piece.lAtackPos.addAll(piece.lPos); // pode atacar todos por onde passar
	}

}
