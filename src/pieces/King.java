package pieces;

import java.util.ArrayList;
import java.util.Iterator;
import javax.swing.JOptionPane;
import model.Square;

public class King extends Piece { // REIS

	/*--------------------------------------------------------------------------------------*/
	// M�TODOS

	// construtor
	public King(Square squareAtual, int group) {
		super(squareAtual, group);
		this.setSquareImage("K");
		this.reach = 1;
	}

	@Override
	protected void moveableUpdate() {
		
		if(this.checkMateNotified) return; // xeque-mate j� notificado (J� ERA!)
		
		// movimento do bispo
		Bishop.bishopMoving(this, 2); // menos de duas casas
		// movimento da torre
		Rook.rookMoving(this, 2); // menos de duas casas
		
		// montagem da matriz de ataque (usada pelo rei rival)
		for(int i=-1; i<=1; i++)
			for(int j=-1; j<=1; j++) {
				if(this.squareAtual.getPosition().x + i >= 0 &&
						this.squareAtual.getPosition().x + i <= 7 &&
						this.squareAtual.getPosition().y + j >= 0 &&
						this.squareAtual.getPosition().y +j <= 7) { // square existe
					this.lAtackPos.add(Piece.getSquareControl().getSquare(
							this.squareAtual.getPosition().x + i,
							this.squareAtual.getPosition().y + j));
					// irrelevante que (0, 0) tamb�m seja contado
				}
			}
		
		this.listClash.clear();
		
		// remo��o de Squares amea�ados
		for(Iterator<Square> it = this.lPos.iterator(); it.hasNext();) {
			Square sqrAux = it.next();
			if(!this.squareVerify(sqrAux)) {
				this.listClash.add(sqrAux);
			}
		}
		this.lPos.removeAll(this.listClash);
		
		// percorrer a lista geral de inimigos e realizar atualiza��es
		for(Iterator<Piece> it = Piece.getEnemyList(this).iterator(); it.hasNext();) {
			Piece enemyAux = it.next();
			if(enemyAux.dead) continue; // pe�a morta
			
			if(enemyAux.moveable(this.squareAtual)) { // Rei em Xeque!
				if(this.checkNotified<2) this.checkNotified = 1; // para ser notificado
				
				// tratamento de Xeque:
				
				// lista todos os Squares entre a pe�a de amea�a e o rei atual
				this.listClash.clear();
				this.listClash = Square.squareBetween(this.squareAtual,
						enemyAux.squareAtual, Piece.getSquareControl());
				this.listClash.add(enemyAux.squareAtual); // o Square do inimigo conta
				
				// eliminar todas as jogadas das demais pe�as que n�o evitam o xeque
				for(Iterator<Piece> fIt = Piece.getFriendList(this).iterator();
						fIt.hasNext();) {
					Piece friendAux = fIt.next();
					if(friendAux==this) continue; // exce��o do pr�prio rei
					
					friendAux.lPos.retainAll(this.listClash); // remover demais
				}
				this.listClash.clear();
				
				// eliminar movimentos do rei que continuariam o xeque
				for(Iterator<Square> sIt = this.lPos.iterator(); sIt.hasNext();) {
					Square squareAux = sIt.next();
					if(Square.squareBetween(squareAux, enemyAux.squareAtual,
							Piece.getSquareControl()).contains(this.squareAtual)) {
						this.listClash.add(squareAux);
					}
				}
				this.lPos.removeAll(this.listClash);
	
			} // fim do tratamento de xeque

			// eliminar jogadas que entregariam o rei:
			this.listClash.clear();
			if(enemyAux.reach > 1) { // apenas para inimigos de longo alcance

				// verificar se existem pe�as amigas entre o inimigo e o rei que evitam amea�as
				this.listClash = Square.squareBetween(this.squareAtual,
						enemyAux.squareAtual, Piece.getSquareControl());

				if(this.listClash.size() > 0) { // existe alinhamento entre o rei e o inimigo

					// verificar se existe apenas uma pe�a amiga no alinhamento:
					ArrayList<Piece> friendList = new ArrayList<Piece>();
					for(Iterator<Piece> frIt = Piece.getFriendList(this).iterator(); frIt.hasNext();) {
						Piece friendAux = frIt.next();
						if(this.listClash.contains(friendAux.squareAtual)) friendList.add(friendAux);
					}
					
					if(friendList.size() == 1) { // condi��o de existencia satisfeita
						if(enemyAux.moveable(friendList.get(0).squareAtual)) { // condi��o de amea�a satisfeita
						
							// se n�o existir nenhuma outra pe�a entre o rei e a pe�a amiga,
							//esta fica proibida de sair do alinhamento
						
							this.listClash.clear();
							this.listClash = Square.squareBetween(this.squareAtual,
									friendList.get(0).squareAtual, Piece.getSquareControl());
							
							boolean test = false;
							while(this.listClash.size()>0) {
								if(this.listClash.get(0).getPieceAtual() != null) test = true;
								this.listClash.remove(0);
							}
							
							if(!test) { // n�o existem outras pe�as: restringir
								
								// novamente...
								this.listClash = Square.squareBetween(this.squareAtual,
										enemyAux.squareAtual, Piece.getSquareControl());
								
								this.listClash.add(enemyAux.squareAtual); // tamb�m conta
								
								friendList.get(0).lPos.retainAll(this.listClash);

							}
						}
					}
				}
			} // fim do tratamento de alinhamentos

		}
				
		// por fim: contagem de todos os movimentos poss�veis
		// se o resultado for ZERO e a rodada � do time atual: CHECKMATE!
		
		int moves = 0; // contador de movimentos poss�veis
		for(Iterator<Piece> fIt = Piece.getFriendList(this).iterator(); fIt.hasNext();) {
			Piece friendAux = fIt.next();
			moves += friendAux.lPos.size();
		}
		
		if(moves==0 && this.getGroupAtual() == this.getGroup()) { // xeque-mate!
			for(Iterator<Piece> fIt = Piece.getFriendList(this).iterator(); fIt.hasNext();) {
				fIt.next().squareAtual.makeLoserCol(); // pintar todos os quadrados amigos de vermelho
				// OBS: a cor vai sumir assim que se passar o cursor por cima (ap�s retir�-lo)
			}
			// marcar como notificado
			this.checkMateNotified = true;
			this.checkNotified = 0; // n�o h� necessidade de informar xeque (se houver)
			// mensagem na tela
			String winName = this.getGroup() == 1? "Brancas" : "Pretas";
			JOptionPane.showMessageDialog(null,
					"Xeque-mate!\n\nPe�as " + winName + " Venceram!",
					"Checkmate!", JOptionPane.INFORMATION_MESSAGE);
		}
		
		// informar ao jogador a posi��o de xeque
		if(this.checkNotified >=2 ) this.checkNotified = 0; // xeque j� notificado
		else if(this.checkNotified == 1) { // xeque ainda n�o notificado
			this.checkNotified = 2;
			String kingName = this.getGroup()==1? "Preto" : "Branco"; // nome usado para an�ncio
			JOptionPane.showMessageDialog(null,"Rei " + kingName + " em Xeque!", "Check!",
					JOptionPane.INFORMATION_MESSAGE);
		}
	}
	
	// Verifica��o de possibilidade de movimento para um Square dado
	public boolean squareVerify(Square square) {

		// percorrer a lista de inimigos e verificar confronto
		for(Iterator<Piece> it = Piece.getEnemyList(this).iterator(); it.hasNext();) {
			Piece enemyAux = it.next();
			
			if(!enemyAux.dead && enemyAux.lAtackPos.contains(square)) return false; // Square sob amea�a

		}

		return true;
	}


	/*--------------------------------------------------------------------------------------*/
	// ATRIBUTOS
	
	// lista os Squares de conflito com pe�as inimigas ao alcance do rei
	private ArrayList<Square> listClash = new ArrayList<Square>();
	
	// verifica se a posi��o de check foi notificada
	private byte checkNotified = 0;
	/*
	 * 0 - n�o h� posi��o de check
	 * 1 - check para ser notificado
	 * 2 - check j� foi ou n�o deve mais ser notificado
	*/
	
	// verifica se o check mate foi notificado
	private boolean checkMateNotified = false;

}
