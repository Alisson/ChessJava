package pieces;

import model.Square;

public class Knight extends Piece { // CAVALOS

	public Knight(Square squareAtual, int group) {
		super(squareAtual, group);
		this.setSquareImage("N");
		this.reach = 1;
	}

	@Override
	protected void moveableUpdate() {
		/* MOVIMENTOS POSS�VEIS:
		 * 
		 *  (-2, -1), (-2, 1), (-1, -2), (-1, 2), (1, -2), (1, 2), (2, -1), (2, 1); 8 ao todo
		 *
		 * */
		
		for(int i=0; i<8; i++) {
			int a, b; // auxiliares de marca��o
			
			// configura��o
			a = 2*(int)(i/4) - 1; // -1 ou 1
			b = 2*(i%2) - 1; // -1 ou 1
			a *= (i%6)>=2? 1 : 2;
			b *= a>0? 3-a : 3+a;
			
			if(this.squareAtual.getPosition().x+a>=0
					&& this.squareAtual.getPosition().x+a<8
					&& this.squareAtual.getPosition().y+b>=0
					&& this.squareAtual.getPosition().y+b<8) {
					// movimento permitido
					
				Piece.target = Piece.getSquareControl().getSquare(
						this.squareAtual.getPosition().x+a,
						this.squareAtual.getPosition().y+b);

				if(Piece.target.getPieceAtual() != null) { // existe uma pe�a no local
					if(this.getGroup()*Piece.target.getPieceAtual().getGroup() == -1) {
						// pe�a inimiga
						this.lPos.add(Piece.target);
					} else {
						// pe�a amiga
						this.lAtackPos.add(Piece.target);
					}
					continue;
				}
					// n�o existe pe�a no local
					this.lPos.add(Piece.target);
			}		
		} // fim de la�o

		this.lAtackPos.addAll(this.lPos); // pode atacar todos por onde passa
	}

}
