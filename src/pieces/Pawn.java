package pieces;

import model.Square;


public class Pawn extends Piece {  // PE�ES

	/*--------------------------------------------------------------------------------------*/
	// M�TODOS

	// construtor
	public Pawn(Square squareAtual, int group) {
		super(squareAtual, group);
		this.setSquareImage("P");
		this.reach = 1;
	}

	@Override
	protected void moveableUpdate() {
		/*----- Verificar se o pe�o pode ser promovido -----*/
		if(!this.promoted && (this.squareAtual.getPosition().x>=7 ||
				this.squareAtual.getPosition().x<=0)) {
			// promover pe�o
			this.promoted = true;
			
			// mudar a imagem para a da rainha
			this.setSquareImage("Q");
		}
		
		if(this.promoted) { // pe�o promovido (RAINHA)
			Bishop.bishopMoving(this, 8);
			Rook.rookMoving(this, 8);
			return;
		}
		
		/*----- Verificar se existe pe�a logo � frente do pe�o -----*/
		Piece.target = Piece.getSquareControl().getSquare(this.squareAtual.getPosition().x
				+ this.getGroup(), this.squareAtual.getPosition().y); // quadrado � frente
		if(Piece.target.getPieceAtual() == null) {
			// n�o h� pe�a � frente do pe�o
			this.lPos.add(Piece.target);
		}
		
		/*----- Verificar se pe�o � virgem caso seja poss�vel mover-se -----*/
		if(this.lPos.size() > 0) { // sem pe�a logo � frente
			
			if(this.getGroup()*this.squareAtual.getPosition().x == -6 ||
					this.getGroup()*this.squareAtual.getPosition().x == 1) { // pe�o virgem
				
				Piece.target = Piece.getSquareControl().getSquare(this.squareAtual.getPosition().x +
						(this.getGroup()*2), this.squareAtual.getPosition().y);
				
				if(Piece.target.getPieceAtual() == null) {
					// n�o existe pe�a dois quadrados � frente do pe�o
					this.lPos.add(Piece.target);
				}
			}
		}
		
		/*----- Verificar existencia de inimigos diagonalmente logo � frente -----*/
		if(this.squareAtual.getPosition().y>0) { // n�o est� no canto esquerdo
			
			Piece.target = Piece.getSquareControl().getSquare(this.squareAtual.getPosition().x
					+this.getGroup(), this.squareAtual.getPosition().y-1); // frente esquerda
			this.lAtackPos.add(Piece.target); // pode atacar neste sentido
			
			if(Piece.target.getPieceAtual() != null) { // existe pe�a na frente esquerda
				if(this.getGroup()*Piece.target.getPieceAtual().getGroup() < 0) { // pe�a inimiga
					this.lPos.add(Piece.target);
				}
			}
		}
		
		if(this.squareAtual.getPosition().y<7) { // n�o est� no canto direito
			
			Piece.target = Piece.getSquareControl().getSquare(this.squareAtual.getPosition().x
					+this.getGroup(), this.squareAtual.getPosition().y+1); // frente direita
			this.lAtackPos.add(Piece.target); // pode atacar neste sentido
			
			if(Piece.target.getPieceAtual() != null) { // existe pe�a na frente direita
				if(this.getGroup()*Piece.target.getPieceAtual().getGroup() < 0) { // pe�a inimiga
					this.lPos.add(Piece.target);
				}
			}
		}
	}
	
	/*--------------------------------------------------------------------------------------*/
	// ATRIBUTOS
	
	// promo��o de um pe�o
	private boolean promoted = false;

}
