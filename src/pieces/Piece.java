package pieces;

import java.util.ArrayList;
import java.util.Iterator;
import control.SquareControl;
import model.Square;


public abstract class Piece { // PE�AS GERAIS

	/*--------------------------------------------------------------------------------------*/
	// DEFINI��O GERAL
	/*
	 * Embora sejam referenciadas nos coment�rios as pe�as como brancas
	 * e pretas, tais refer�ncias servem apenas para diferenciar as duas fac��es;
	 * pois, embora se possa mudar as cores das pe�as que ser�o usadas (colorOne
	 * e colorTwo em SquareBoardPanel), as pe�as inferiores sempre ter�o o
	 * diferencial de come�arem o jogo.
	 * 
	 * */

	/*--------------------------------------------------------------------------------------*/
	// M�TODOS
	
	// construtor
	public Piece(Square squareAtual, int group) {
		// sem a informa��o de um Square ou grupo n�o � poss�vel criar a pe�a
		super();
		this.squareAtual = squareAtual;
		if(group == 1) {
			Piece.blackPieces.add(this); // pe�a preta
			this.group = 1;
		} else {
			Piece.whitePieces.add(this); // pe�a branca (por default)
			this.group = -1;
		}
	}
	
	// devolve a lista de possiveis locais a se mover
	public ArrayList<Square> getlPos() {
		return lPos;
	}
	
	// verifica se a pe�a pode se mover para um dado Square (ser� usada pelos reis)
	public boolean moveable(Square square) {
		return this.lPos.contains(square);
	}
	
	// verifica se a pe�a pode ser usada para o pr�ximo movimento
	public boolean usable() {
		if(Piece.groupAtual != this.group) return false; // vez do outro jogador
		return (this.lPos.size() > 0); // verifica se tem para onde ir
	}
	
	// mover para um Square dado (se possivel, caso n�o: retorna false)
	public boolean moveToSquare(Square square) {
		// verificar se pode mover-se
		if(!this.moveable(square)) return false;
		
		this.resetPossibCol();
		
		this.squareAtual.setPieceAtual(null); // desvincula a pe�a com o Square atual
		this.squareAtual.setImagePath(""); // limpa o square atual
		
		if(square.getPieceAtual() != null) { // existia uma pe�a inimiga no local (remover)
			square.getPieceAtual().dead = true; // mata a pe�a
			if(this.group < 1) // pe�a atual � branca
				Piece.blackPieces.remove(square.getPieceAtual());
			else
				Piece.whitePieces.remove(square.getPieceAtual());
		}
		
		square.setPieceAtual(this); // vincula a pe�a com o Square informado
		this.squareAtual = square; // vincula o Square informado com a pe�a
		
		Piece.groupAtual *= (-1); // passa a vez para o advers�rio
		Piece.squareControl.resetColor(this.squareAtual); // evita a possibilidade de mover novamente a pe�a
		
		// atualizar todas as pe�as do tabuleiro
		Piece.generalUpdate();
		
		return true;
	} // fim do movimento
	
	// grupo (ou fac��o da pe�a)
	public int getGroup() {
		return group;
	}
	
	// rodada atual
	public int getGroupAtual() {
		return Piece.groupAtual;
	}

	// lista de inimigos de uma pe�a
	public static ArrayList<Piece> getEnemyList(Piece piece) {
		return (piece.group==1)? Piece.whitePieces : Piece.blackPieces;
	}

	// lista de amigos de uma pe�a
	public static ArrayList<Piece> getFriendList(Piece piece) {
		return (piece.group==-1)? Piece.whitePieces : Piece.blackPieces;
	}
	
	// controle para acesso a diversos Squares
	public static void setSquareControl(SquareControl squareControl) {
		// s� � permitido alterar uma vez
		Piece.squareControl = squareControl;
	}
	
	// informa o controle
	public static SquareControl getSquareControl() {
		return Piece.squareControl;
	}
	
	// atualiza��o geral das pe�as
	public static void generalUpdate() {
		Piece auxiliar;
		
		// percorrer as listas de pe�as e atualizar seus possiveis caminhos
		for(Iterator<Piece> it = Piece.whitePieces.iterator(); it.hasNext();) {
			auxiliar = it.next();
			auxiliar.lPos.clear(); // limpa toda a lista
			auxiliar.lAtackPos.clear(); // limpa toda a lista
			auxiliar.moveableUpdate(); // atualiza pe�as brancas
		}
		for(Iterator<Piece> it = Piece.blackPieces.iterator(); it.hasNext();) {
			auxiliar = it.next();
			auxiliar.lPos.clear(); // limpa toda a lista
			auxiliar.lAtackPos.clear(); // limpa toda a lista
			auxiliar.moveableUpdate(); // atualiza pe�as pretas
		}
		
		// percorrer novamente a listas de pe�as brancas (adapta��o para os reis)
		for(Iterator<Piece> it = Piece.whitePieces.iterator(); it.hasNext();) {
			auxiliar = it.next();
			auxiliar.lPos.clear(); // limpa toda a lista
			auxiliar.lAtackPos.clear(); // limpa toda a lista
			auxiliar.moveableUpdate(); // reatualiza pe�as brancas
		}

	}
		
	// define as cores superior e inferior (respectivamente) das pe�as
	public static void setColorPieces(String supCol, String infCol) {
		Piece.supCol = supCol;
		Piece.infCol = infCol;
	}
	
	// retorna a cor da pe�a
	protected String getThisColor() {
		return (this.group==1? Piece.supCol : Piece.infCol);
	}
	
	// define a imagem que ser� colocada no local, informada a pe�a (uma letra correspondente)
	protected void setSquareImage(String info) {
		this.squareAtual.setImagePath("icon/" + this.getThisColor() + " " + info + "_48x48.png");
	}
	
	// reset das cores dos poss�veis locais para onde a pe�a pode se mover
	public void resetPossibCol() {
		for(Iterator<Square> it = this.lPos.iterator(); it.hasNext();) {
			Square sqrAux = it.next();
			sqrAux.setColor(Piece.squareControl.getGridColor(sqrAux.getPosition().x,
					sqrAux.getPosition().y));
		}
	}
	
	/*--------------------------------------------------------------------------------------*/
	// ATRIBUTOS
	
	// Square sobre o qual a pe�a se encontra atualmente
	protected Square squareAtual;
	
	// lista de todos os Squares para os quais se pode mover
	protected ArrayList<Square> lPos = new ArrayList<Square>();
	
	// lista de todos os Squares para os quais se pode mover para atacar (incluindo amigos)
	// (usado pelo rei para evitar o suicidio)
	protected ArrayList<Square> lAtackPos = new ArrayList<Square>();
	
	// fac��o � qual pertence a pe�a (1: pretas, -1: brancas)
	private int group;
	
	// cor aplicada � pe�a
	protected String pieceCol = new String();
	
	// controle que permite acessar Squares diferentes do de permanencia
	private static SquareControl squareControl = null;

	// listas gerais de pe�as brancas e pretas (inferiores e superiores, respectivamente)
	protected static ArrayList<Piece> whitePieces = new ArrayList<Piece>(); // inferiores
	protected static ArrayList<Piece> blackPieces = new ArrayList<Piece>(); // superiores
	
	// indicador de quem deve jogar atualmente (1: pretas, -1: brancas)
	private static int groupAtual = -1;
	
	// auxiliar para opera��o com Squares
	protected static Square target = null;
	
	// armazenamento das cores superior e inferior das pe�as
	protected static String supCol = new String();
	protected static String infCol = new String();
	
	// adapta��o ao erro de continuidade de check
	protected boolean dead = false;
	
	// informa��o de alcance de uma pe�a (com exce��o do cavalo, que n�o usa)
	protected int reach = 8; // valor de default (reis e pe�es o modificam)
	
	/*--------------------------------------------------------------------------------------*/
	// ABSTRATOS

	// atualiza a lista de Squares para os quais pode se mover
	protected abstract void moveableUpdate();
	
}
