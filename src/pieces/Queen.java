package pieces;

import model.Square;

public class Queen extends Piece { // RAINHAS

	public Queen(Square squareAtual, int group) {
		super(squareAtual, group);
		this.setSquareImage("Q");
	}

	@Override
	protected void moveableUpdate() {
		// movimento do bispo
		Bishop.bishopMoving(this, 8); // todo o tabuleiro
		// movimento da torre
		Rook.rookMoving(this, 8); // todo o tabuleiro
	}

}
