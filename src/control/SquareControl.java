package control;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Iterator;

import model.Square;
import model.Square.SquareEventListener;

public class SquareControl implements SquareEventListener {

	public static final int ROW_NUMBER = 8;
	public static final int COL_NUMBER = 8;

	public static final Color DEFAULT_COLOR_ONE = Color.WHITE;
	public static final Color DEFAULT_COLOR_TWO = Color.DARK_GRAY;
	public static final Color DEFAULT_COLOR_HOVER = Color.ORANGE;
	public static final Color DEFAULT_COLOR_SELECTED = Color.GREEN;
	public static final Color DEFAULT_COLOR_NOT_ONE = new Color(208, 31, 31);
	public static final Color DEFAULT_COLOR_NOT_TWO = new Color(100, 15, 15);

	public static final Square EMPTY_SQUARE = null;

	private Color colorOne;
	private Color colorTwo;
	private Color colorHover;
	private Color colorSelected;
	private Color colorNotOne;
	private Color colorNotTwo;

	private Square selectedSquare;
	private ArrayList<Square> squareList;

	public SquareControl() {
		this(DEFAULT_COLOR_ONE, DEFAULT_COLOR_TWO, DEFAULT_COLOR_HOVER,
				DEFAULT_COLOR_SELECTED, DEFAULT_COLOR_NOT_ONE,
				DEFAULT_COLOR_NOT_TWO);
	}

	public SquareControl(Color colorOne, Color colorTwo, Color colorHover,
			Color colorSelected, Color colorNotOne, Color colorNotTwo) {
		this.colorOne = colorOne;
		this.colorTwo = colorTwo;
		this.colorHover = colorHover;
		this.colorSelected = colorSelected;
		this.colorNotOne = colorNotOne;
		this.colorNotTwo = colorNotTwo;

		this.squareList = new ArrayList<>();
		createSquares();
	}

	public void resetColor(Square square) {
		if(square.getColor() == Square.POSSIB_COL_TREE) {
			square.makePossibCol();
			return;
		}
		
		if(square.getColor() == Square.POSSIB_COL_ONE ||
				square.getColor() == Square.POSSIB_COL_TWO ||
				square.getColor() == Square.LOSER_COL_ONE ||
				square.getColor() == Square.LOSER_COL_TWO) return; // cores n�o alteradas levianamente
		
		int index = this.squareList.indexOf(square);
		int row = index / COL_NUMBER;
		int col = index % COL_NUMBER;

		square.setColor(getGridColor(row, col));
	}

	@Override
	public void onHoverEvent(Square square) {
		// Verificar se o quadrado pode ser selecionado:
		if(square.getColor()==Square.LOSER_COL_ONE ||
				square.getColor()==Square.LOSER_COL_TWO) return; // n�o modifica cores de pe�as que perderam
		
		if (this.selectedSquare != EMPTY_SQUARE) {	
			// existe uma pe�a j� selecionada (verificar se pode se mover para square)
			
			if(this.selectedSquare.getPieceAtual().moveable(square))
				square.setColor(Square.POSSIB_COL_TREE);
			else if(this.selectedSquare != square)	square.makeNotColor();
				
		} else if(square.getPieceAtual() != null) {
			// existe uma pe�a no quadrado (primeira sele��o)
				if(square.getPieceAtual().usable())
					square.setColor(this.colorHover); // cor da vez (pe�a autorizada a mover-se)
				else
					square.makeNotColor();
		} else {
			// selecionando um square vazio
			square.makeNotColor();
		}
	}

	@Override
	public void onSelectEvent(Square square) {
		// Verificar se o quadrado � permitido:
		if(square.getColor()==this.colorNotOne ||
				square.getColor()==this.colorNotTwo ||
				square.getColor()==this.colorOne ||
				square.getColor()==this.colorTwo) return; // irregularidade na sele��o
		
		if (haveSelectedCellPanel()) {
			// Mover normalmente
			moveContentOfSelectedSquare(square);
		} else {
			// Selecionar normalmente
			selectSquare(square);
			// listar os Squares possiveis para se mover
			for(Iterator<Square> it = square.getPieceAtual().getlPos().iterator();
					it.hasNext();) {
				it.next().makePossibCol();
			}
			
		}
	}

	@Override
	public void onOutEvent(Square square) {
		if (this.selectedSquare != square) {
			resetColor(square);
		} else {
			square.setColor(this.colorSelected);
		}
	}

	public Square getSquare(int row, int col) {
		return this.squareList.get((row * COL_NUMBER) + col);
	}

	public ArrayList<Square> getSquareList() {
		return this.squareList;
	}

	public Color getGridColor(int row, int col) {
		if ((row + col) % 2 == 0) {
			return this.colorOne;
		} else {
			return this.colorTwo;
		}
	}

	private void addSquare() {
		Square square = new Square();
		this.squareList.add(square);
		resetColor(square);
		resetPosition(square);
		square.setSquareEventListener(this);
	}

	private void resetPosition(Square square) {
		int index = this.squareList.indexOf(square);
		int row = index / COL_NUMBER;
		int col = index % COL_NUMBER;

		square.getPosition().setLocation(row, col);
	}

	private boolean haveSelectedCellPanel() {
		return this.selectedSquare != EMPTY_SQUARE;
	}

	private void moveContentOfSelectedSquare(Square square) {
		square.setImagePath(this.selectedSquare.getImagePath());
		resetColor(this.selectedSquare);
		
		if(this.selectedSquare != square) {
			// um square diferente foi selecionado -> mover conteudo
			
			if(this.selectedSquare.getPieceAtual().moveToSquare(square))
				this.selectedSquare.removeImage();
			
		} else {
			// mesmo square selecionado: desmarcar
			this.selectedSquare.getPieceAtual().resetPossibCol();
			//this.selectedSquare.setColor(colorHover);
		}
		this.selectedSquare = EMPTY_SQUARE;
	}

	private void selectSquare(Square square) {
		if (square.haveImagePath()) {
			this.selectedSquare = square;
			this.selectedSquare.setColor(this.colorSelected);
		}
	}

	private void createSquares() {
		for (int i = 0; i < (ROW_NUMBER * COL_NUMBER); i++) {
			addSquare();
		}
	}
}
