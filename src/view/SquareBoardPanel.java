package view;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.ArrayList;

import javax.swing.JPanel;

import pieces.Bishop;
import pieces.King;
import pieces.Knight;
import pieces.Pawn;
import pieces.Piece;
import pieces.Queen;
import pieces.Rook;

import model.Square;
import control.SquareControl;

public class SquareBoardPanel extends JPanel {

	private static final long serialVersionUID = 7332850110063699836L;

	private SquareControl squareControl;
	private ArrayList<SquarePanel> squarePanelList;

	public SquareBoardPanel() {
		setLayout(new GridBagLayout());
		this.squarePanelList = new ArrayList<SquarePanel>();

		initializeSquareControl();
		initializeGrid();
		initializePiecesInChess();
	}

	private void initializeSquareControl() {
		Color colorOne = Color.WHITE;
		Color colorTwo = Color.DARK_GRAY;
		Color colorHover = Color.ORANGE;
		Color colorSelected = Color.GREEN;
		Color colorNotOne = SquareControl.DEFAULT_COLOR_NOT_ONE;
		Color colorNotTwo = SquareControl.DEFAULT_COLOR_NOT_TWO;
		

		this.squareControl = new SquareControl(colorOne, colorTwo, colorHover,
				colorSelected, colorNotOne, colorNotTwo);
	}

	private void initializeGrid() {
		GridBagConstraints gridBag = new GridBagConstraints();

		Square square;
		for (int i = 0; i < this.squareControl.getSquareList().size(); i++) {
			square = this.squareControl.getSquareList().get(i);
			gridBag.gridx = square.getPosition().y;
			gridBag.gridy = square.getPosition().x;

			SquarePanel squarePanel = new SquarePanel(square);

			add(squarePanel, gridBag);
			this.squarePanelList.add(squarePanel);
		}

	}

	// inicializa��o das pe�as no tabuleiro
	private void initializePiecesInChess() {
		
		Piece.setSquareControl(this.squareControl);
		
		Square target;
		String colorOne = "Black"; // cor das pe�as superiores
		String colorTwo = "White"; // cor das pe�as inferiores (Pe�as que come�am)
		
		// set das cores de pe�as
		Piece.setColorPieces(colorOne, colorTwo);
		

		for (int i = 0; i<8; i++) { // PE�ES
			// superiores
			target = this.squareControl.getSquare(1, i);
			target.setPieceAtual(new Pawn(target, 1));
			// inferiores
			target = this.squareControl.getSquare(6, i);
			target.setPieceAtual(new Pawn(target, -1));
		}
		
		for(int i=0; i<2; i++) { // PE�AS DE PARES
			// torres superiores
			target = this.squareControl.getSquare(0, 7*i);
			target.setPieceAtual(new Rook(target, 1));
			// torres inferiores
			target = this.squareControl.getSquare(7, 7*i);
			target.setPieceAtual(new Rook(target, -1));
			
			// cavalos superiores
			target = this.squareControl.getSquare(0, 5*i+1);
			target.setPieceAtual(new Knight(target, 1));
			// cavalos inferiores
			target = this.squareControl.getSquare(7, 5*i+1);
			target.setPieceAtual(new Knight(target, -1));
			
			// bispos superiores
			target = this.squareControl.getSquare(0, 3*i+2);
			target.setPieceAtual(new Bishop(target, 1));
			// bispos inferiores
			target = this.squareControl.getSquare(7, 3*i+2);
			target.setPieceAtual(new Bishop(target, -1));
		}
		
		// REIS E RAINHAS
		// rainha superior
		target = this.squareControl.getSquare(0, 3);
		target.setPieceAtual(new Queen(target, 1));
		// rainha inferior
		target = this.squareControl.getSquare(7, 3);
		target.setPieceAtual(new Queen(target, -1));

		// rei superior
		target = this.squareControl.getSquare(0, 4);
		target.setPieceAtual(new King(target, 1));
		// rei inferior
		target = this.squareControl.getSquare(7, 4);
		target.setPieceAtual(new King(target, -1));
		
		// OBS: Reis s�o sempre inseridos por �ltimo
		
		Piece.generalUpdate();
	}
}
